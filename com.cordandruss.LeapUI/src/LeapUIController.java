import processing.core.PApplet;

import java.lang.reflect.Method;

public class LeapUIController {
    PApplet parent;

    public LeapUIController(PApplet parent) {
        this.parent = parent;
        parent.registerMethod("dispose", this);
    }

    public void dispose() {

    }
}
