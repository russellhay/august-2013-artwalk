/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */

package SoundWorld;

import AugustArtwalk.AugustArtwalk;
import AugustArtwalk.CollisionObject;
import AugustArtwalk.PGraphicAdapter;
import com.sun.org.apache.xpath.internal.functions.FuncFalse;
import ddf.minim.AudioSample;
import fisica.FCircle;
import processing.core.PGraphics;
import processing.core.PVector;

public class SoundObject extends FCircle implements CollisionObject
{
    float volume;
    float currentVolume;
    int color;
    int currentLifeSpan;
    AudioSample sample;
    SoundWorld sWorld;

    static boolean played = false;
    private float lastPlayedTime;

    public SoundObject(SoundWorld sWorld, PVector position, PVector velocity, int color, float volume, AudioSample sample) {
        super(volume);
        this.setPosition(position.x, position.y);
        this.setVelocity(velocity.x, velocity.y);
        this.setDensity(volume);
        this.volume = volume;
        this.color = color;
        this.currentVolume = volume;
        this.currentLifeSpan = 0;
        this.sample = sample;
        this.sWorld = sWorld;
    }

    public void onCollision(CollisionObject obj) {
        currentLifeSpan += 1;
    }

    @Override
    public void draw(PGraphics app)
    {
        PGraphicAdapter g = new PGraphicAdapter(app);
        g.stroke(sWorld.mainColor.toARGB());
        g.strokeWeight(5);
        g.fill(sWorld.getColor(color));
        g.circle(this.getX(), this.getY(), (int)currentVolume);
        g.noFill();
        g.circle(this.getX(), this.getY(), (int)volume);
        currentLifeSpan += 1;
    }

    public boolean alive() {
        return currentLifeSpan < AugustArtwalk.TIME_TO_LIVE;
    }

    public void update() {
        currentLifeSpan += 1;
//        currentVolume = (int)PApplet.lerp(volume, 5, (float)currentLifeSpan / (float)TIME_TO_LIVE);
//        this.setSize(currentVolume);
    }

    @Override
    public PVector collisionForce()
    {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void play(float time)
    {
        if (played && time - lastPlayedTime > AugustArtwalk.PLAY_FREQUENCY) {
            played = false;
        }

        if (!played) {
            sample.trigger();
            played = true;
            lastPlayedTime = time;
        }
    }
}
