/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */

package SoundWorld;

import AugustArtwalk.AugustArtwalk;
import AugustArtwalk.RandomHelper;
import controlP5.*;
import ddf.minim.AudioSample;
import ddf.minim.Minim;
import fisica.FBody;
import fisica.FContact;
import fisica.FWorld;
import fisica.Fisica;
import processing.core.PApplet;
import processing.core.PVector;
import toxi.color.ColorList;
import toxi.color.ColorRange;
import toxi.color.TColor;
import toxi.color.theory.ColorTheoryRegistry;
import toxi.color.theory.ColorTheoryStrategy;

import java.util.ArrayList;
import java.util.Iterator;

public class SoundWorld implements ControlListener
{
    private static final long MEGABYTE = 1024 * 1024;
    public static float MEMORY_UPDATE_TIME = 1000;
    public static float COLOR_SWITCH_TIME = 1000 * 60 * 2.5f;
    private static final int SWATCH_SIZE = 10;
    AugustArtwalk parent;
    RandomHelper helper;
    AutoRecorder recorder;
    ArrayList<SoundObject> sounds;
    final FWorld world;
    public final Minim minim;
    private final ControlP5 controls;
    public TColor mainColor;
    TColor backgroundColor;
    ColorList colors;
    Chart memoryChart;
    Runtime runtime;
    float lastMemoryTime = 0;
    float lastColorSwitchTime = 0;
    ColorTheoryStrategy[] theories = new ColorTheoryStrategy[] {
        ColorTheoryRegistry.RIGHT_SPLIT_COMPLEMENTARY,
        ColorTheoryRegistry.TETRAD,
        ColorTheoryRegistry.ANALOGOUS,
        ColorTheoryRegistry.COMPOUND,
        ColorTheoryRegistry.LEFT_SPLIT_COMPLEMENTARY,
        ColorTheoryRegistry.MONOCHROME,
//        ColorTheoryRegistry.SINGLE_COMPLEMENT,
//        ColorTheoryRegistry.SPLIT_COMPLEMENTARY,
        ColorTheoryRegistry.TRIAD
    };
    private Textlabel colorTimeLabel;
    public float colorTime = 0;

    public void contactStarted(FContact contact)
    {
        FBody b1 = contact.getBody1();
        FBody b2 = contact.getBody2();

        if (SoundObject.class.isInstance(b1) /* && !SoundObject.class.isInstance(b2) */)
        {
            ((SoundObject) b1).play(parent.millis());
        } /* else */
        if (SoundObject.class.isInstance(b2) /* && !SoundObject.class.isInstance(b1) */)
        {
            ((SoundObject) b2).play(parent.millis());
        }

    }

    public int getColor(int i) {
        return colors.get(i%colors.size()).toARGB();
    }

    public void contactEnded(FContact contact)
    {
    }

    public SoundWorld(AugustArtwalk parent)
    {
        this.parent = parent;
        controls = new ControlP5(parent);
        createControls();


        minim = new Minim(parent);
        recorder = new AutoRecorder(parent, this, minim.getLineIn(), 0, parent.height / 2);
        Fisica.init(parent);
        world = new FWorld();
        world.setEdges(255);
        world.setEdgesRestitution(1f);
        world.setEdgesFriction(0.05f);
        world.setGrabbable(true);

        helper = (RandomHelper) parent;
        sounds = new ArrayList<SoundObject>();
        newColorSet();
        runtime = Runtime.getRuntime();
    }

    public void newColorSet()
    {
        int randomTheory = (int)parent.random(theories.length);
        mainColor = ColorRange.FRESH.getColor();
        PApplet.println(theories[randomTheory].getName());
        colors = ColorList.createUsingStrategy(theories[randomTheory], mainColor);
        backgroundColor = colors.getDarkest();
    }

    private void createControls()
    {
        int xPosition = 25;
        controls.addSlider("PLAY_FREQUENCY")
                .setRange(250, 2000)
                .setPosition(xPosition, 50)
                .setCaptionLabel("Sample Frequency");
        controls.addSlider("TIME_TO_LIVE")
                .setRange(60 * 60, 60 * 60 * 10)
                .setPosition(xPosition, 70)
                .setCaptionLabel("Sound Object Duration");

        controls.addSlider("VOLUME_THRESHOLD")
                .setRange(5, 100)
                .setPosition(xPosition, 90)
                .setCaptionLabel("Volume Autorecord Threshold");
        controls.addSlider("RECORD_DURATION")
                .setRange(250, 3000)
                .setPosition(xPosition, 110)
                .setCaptionLabel("Record Max Duration");
        controls.addSlider("SILENT_DURATION")
                .setRange(100, 1000)
                .setPosition(xPosition, 130)
                .setCaptionLabel("Silent Duration to end");
        controls.addSlider("LOCKOUT_DURATION")
                .setRange(500, 5000)
                .setPosition(xPosition, 150)
                .setCaptionLabel("Record Lockout Period");
        controls.addSlider("RANDOM_FORCE")
                .setRange(200, 5000)
                .setPosition(xPosition, 170)
                .setCaptionLabel("Random Force on ENTER");

        colorTimeLabel = controls.addTextlabel("colorTime")
                .setPosition(xPosition, 190)
                .setCaptionLabel("Color Duration");
//        memoryChart = controls.addChart("MEMORY")
//                .setPosition(xPosition, parent.height - 120)
//                .setSize(200, 100)
//                .setRange(0, 1000000000)
//                .setView(Chart.AREA)
//                .setCaptionLabel("Memory Usage");
//
//        memoryChart.addDataSet("used");
//        memoryChart.setColors("used", parent.color(255), parent.color(0, 255, 0));
    }

    public void clear()
    {
        for (SoundObject s : sounds)
        {
            world.remove(s);
        }
        sounds.clear();
    }

    public void addParticle(AudioSample sound)
    {

        float weight = parent.random(parent.height / 15) * 5 + 25;
        PVector position = helper.randomVector();
        position.limit(parent.width - weight);
        if (position.dist(new PVector(0, 0)) < weight)
        {
            position.x = weight;
            position.y = weight;
        }
        PVector velocity = helper.randomUnitVector();
        velocity.mult(weight);
        SoundObject s = new SoundObject(
                this,
                position,
                velocity,
                (int)parent.random(colors.size()),
                weight,
                sound
        );
        s.setDamping(0.01f);
        s.setFriction(0.01f);
        sounds.add(s);
        world.add(s);
    }

    public void update()
    {
        world.step();
//        if (parent.millis() - lastMemoryTime > MEMORY_UPDATE_TIME)
//        {
//            lastMemoryTime = parent.millis();
//            long usedMemory = runtime.totalMemory() - runtime.freeMemory() / MEGABYTE;
//            memoryChart.push("used", (float) usedMemory);
//        }
        colorTime = parent.millis() - lastColorSwitchTime;
        colorTimeLabel.setText("Time Left Until Color Switch: " + ((COLOR_SWITCH_TIME - colorTime)/60000));
        if (colorTime > COLOR_SWITCH_TIME)
        {
            lastColorSwitchTime = parent.millis();
            newColorSet();
        }
    }

    private void removeDeadSounds()
    {
        Iterator<SoundObject> i = sounds.iterator();
        while (i.hasNext())
        {
            SoundObject s = i.next();
            if (!s.alive())
            {
                world.remove(s);
                i.remove();
            }
        }
        recorder.update();
    }

    public void draw()
    {
        parent.background(backgroundColor.toARGB());
        world.draw();
        for (SoundObject s : sounds)
        {
            s.update();
        }
        removeDeadSounds();
        recorder.draw();
        if (controls.isVisible()) {
            drawColorRange();
        }
    }

    private void drawColorRange()
    {
        int colorsCount = colors.size();
        parent.noStroke();
        parent.pushMatrix();
        parent.translate(parent.width - (colorsCount+2)*SWATCH_SIZE, SWATCH_SIZE);
        for(int i=0; i< colorsCount; i++) {
            parent.translate(SWATCH_SIZE, 0);
            parent.fill(colors.get(i).toARGB());
            parent.rect(0, 0, SWATCH_SIZE, SWATCH_SIZE);
        }
        parent.popMatrix();
    }

    @Override
    public void controlEvent(ControlEvent event)
    {
        PApplet.println(event.getName() + " " + event.getValue());
    }

    public void toggleControls()
    {
        if (controls.isVisible())
        {
            controls.hide();
        } else
        {
            controls.show();
        }
    }

    public void randomImpulse()
    {
        for (SoundObject s : sounds)
        {
            s.addImpulse(parent.random(-AugustArtwalk.RANDOM_FORCE, AugustArtwalk.RANDOM_FORCE),
                    parent.random(-AugustArtwalk.RANDOM_FORCE, AugustArtwalk.RANDOM_FORCE));
        }
    }

    public void save()
    {
        controls.saveProperties();
    }

    public void load()
    {
        controls.loadProperties();
    }
}
