/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */

package SoundWorld;

import AugustArtwalk.AugustArtwalk;
import ddf.minim.AudioRecorder;
import ddf.minim.AudioSample;
import ddf.minim.AudioSource;
import ddf.minim.spi.AudioRecordingStream;
import ddf.minim.spi.SampleRecorder;
import processing.core.PApplet;

import java.util.ArrayList;

public class AutoRecorder
{
    SoundWorld world;
    private final AugustArtwalk parent;
    private final AudioSource in;
    private AudioRecorder recorder;
    boolean recording, silent, lockout;
    String recordName;
    float startLockOut;
    float startRecordingTime, startLowTime, x, y;

    public AutoRecorder(AugustArtwalk parent, SoundWorld world, AudioSource in, float x, float y) {
        this.parent = parent;
        this.world = world;
        this.in = in;
        recording = false;
        this.x = x;
        this.y = y;
        lockout = false;
    }

    public void update() {
        if (recording) {
            float currentLevel = in.mix.level()*1000;

            if (!silent && currentLevel < AugustArtwalk.VOLUME_THRESHOLD) {
                startLowTime = parent.millis();
            } else if (silent && currentLevel > AugustArtwalk.VOLUME_THRESHOLD) {
                silent = false;
            }
            if ((parent.millis() - startRecordingTime > AugustArtwalk.RECORD_DURATION) || (
                    silent && parent.millis() - startLowTime > AugustArtwalk.SILENT_DURATION )) {
                recorder.save();
                recorder.endRecord();
                world.addParticle(world.minim.loadSample(recordName));
                lockout = true;
                startLockOut = parent.millis();
                recording = false;
            }
            return;
        }

        if (lockout && parent.millis()-startLockOut > AugustArtwalk.LOCKOUT_DURATION) {
            lockout = false;
        }

        if (in.mix.level()*1000 > AugustArtwalk.VOLUME_THRESHOLD && !lockout) {
            recording = true;
            silent = false;
            recordName = "data/sample" + parent.millis() + ".wav";
            recorder = world.minim.createRecorder(in, recordName, false);
            startRecordingTime = parent.millis();
            recorder.beginRecord();
        }
    }

    public void draw() {
        if (recording) {
            parent.fill(world.mainColor.toARGB());
            parent.noStroke();
            float px, duration;
            duration = in.mix.level()*1000;
            px = x+duration/2;
            parent.ellipse(px, y, duration, duration);
        }
    }
}
