/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */

package AugustArtwalk;

import processing.core.PVector;

public interface CollisionObject
{
    PVector collisionForce();
}
