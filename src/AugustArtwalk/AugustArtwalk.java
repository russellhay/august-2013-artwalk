/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */

package AugustArtwalk;

import fisica.FContact;
import processing.core.PApplet;
import processing.core.PGraphics;

import java.util.ArrayList;
import SoundWorld.SoundWorld;
import processing.core.PImage;
import processing.core.PVector;

public class AugustArtwalk extends PApplet implements DrawingObject, RandomHelper {
    SoundWorld world;

    public static float PLAY_FREQUENCY = 750;
    public static int TIME_TO_LIVE = 60 * 60 * 4; // LifeSpan ticks on collisions with other balls, and at every frame
    public static float VOLUME_THRESHOLD = 75;
    public static float RECORD_DURATION = 1000;
    public static float SILENT_DURATION = 250;
    public static float LOCKOUT_DURATION = 1000;
    public static float RANDOM_FORCE = 200;


    public void setup() {
        size(1024, 768);
        frameRate(60);

        ellipseMode(CENTER);
        world = new SoundWorld(this);
    }

    public void contactStarted(FContact contact) {
        world.contactStarted(contact);
    }

    public void contactEnded(FContact contact) {
        world.contactEnded(contact);
    }

    public void keyPressed() {
        if (key == '`') {
            world.toggleControls();
        } else if (key == ' ') {
            world.clear();
        } else if (key == ENTER) {
            world.randomImpulse();
        } else if (key == 'c') {
            world.newColorSet();
        } else if (key == 's') {
            world.save();
        } else if (key == 'l') {
            world.load();
        }
    }

    public void draw() {
        world.update();
        world.draw();
    }

    @Override
    public void image(PGraphics screen, float opacity)
    {
        tint(255, opacity);
        image(screen.get(0, 0, screen.width, screen.height), 0, 0);
    }

    @Override
    public void circle(float x, float y, int radius)
    {
        ellipse(x, y, radius, radius);
    }

    @Override
    public float width()
    {
        return width;
    }

    @Override
    public float height()
    {
        return height;
    }

    @Override
    public float halfWidth()
    {
        return width/2f;
    }

    @Override
    public float halfHeight()
    {
        return height/2f;
    }

    @Override
    public PGraphics asPGraphic()
    {
        return null;
    }

    @Override
    public void strokeWeight(int i)
    {
        strokeWeight(i);
    }

    @Override
    public int randomColor()
    {
        return color(random(0,255), random(0, 255), random(0, 255));
    }

    @Override
    public PVector randomUnitVector()
    {
        PVector retval = new PVector(random(width), random(height));
        retval.normalize();

        return retval;
    }

    @Override
    public PVector randomVector()
    {
        return new PVector(random(width), random(height/2));
    }
}
