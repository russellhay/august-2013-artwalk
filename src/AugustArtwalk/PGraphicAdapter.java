package AugustArtwalk;

import processing.core.PGraphics;
import processing.core.PVector;


public class PGraphicAdapter implements DrawingObject
{
    PGraphics parent;

    public PGraphicAdapter(PGraphics parent) {
        this.parent = parent;
    }

    @Override
    public void background(float r, float g, float b)
    {
        parent.background(r, g, b);
    }

    @Override
    public void background(float gray)
    {
        parent.background(gray);
    }

    @Override
    public void image(PGraphics screen, float opacity)
    {
        parent.tint(255, opacity);
        parent.image(screen.get(0, 0, screen.width, screen.height), 0, 0);
    }

    @Override
    public void stroke(int color)
    {
        parent.stroke(color);
    }

    @Override
    public void circle(float x, float y, int radius)
    {
        parent.ellipse(x, y, radius, radius);
    }

    @Override
    public void fill(int color)
    {
        parent.fill(color);
    }

    @Override
    public float width()
    {
        return parent.width;
    }

    @Override
    public float height()
    {
        return parent.height;
    }

    @Override
    public void pushMatrix()
    {
        parent.pushMatrix();
    }

    @Override
    public float halfWidth()
    {
        return parent.width/2f;
    }

    @Override
    public float halfHeight()
    {
        return parent.height/2f;
    }

    @Override
    public void translate(float x, float y)
    {
        parent.translate(x, y);
    }

    @Override
    public void popMatrix()
    {
        parent.popMatrix();
    }

    @Override
    public void noFill()
    {
        parent.noFill();
    }

    @Override
    public PGraphics asPGraphic()
    {
        return parent;
    }

    @Override
    public void noStroke()
    {
        parent.noStroke();
    }

    @Override
    public void strokeWeight(int i)
    {
        parent.strokeWeight(i);
    }
}
