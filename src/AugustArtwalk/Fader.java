/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */

package AugustArtwalk;

import processing.core.*;

public class Fader implements ArtObject {
    PGraphics frontScreen, backScreen;
    PGraphicAdapter frontAdapter, backAdapter;
    ArtObject front, back;
    PApplet parent;
    boolean fading;
    float fadeStart;
    static float FADE_DURATION = 5000f;

    public Fader(PApplet p) {
        this.parent = p;
        frontScreen = p.createGraphics(p.width, p.height, p.sketchRenderer());
        backScreen = p.createGraphics(p.width, p.height, p.sketchRenderer());
        frontAdapter = new PGraphicAdapter(frontScreen);
        backAdapter = new PGraphicAdapter(backScreen);
        fadeStart = -1;
        front = null;
        back = null;
        fading = false;
    }

    public void set(ArtObject front) {
        fading = false;
        this.front = front;
    }

    public void fadeTo(ArtObject back) {
        if (fading) {
            front.onPause();
            front = this.back;
        }

        this.back = back;
        back.onResume();
        fading = true;
        fadeStart = parent.millis();
    }

    @Override
    public void update(DrawingObject g) {
        if (fading) {
            front.update(g);
            back.update(g);
        } else if (front != null) {
            front.update(g);
        }
    }

    @Override
    public void draw(DrawingObject g) {
        if (fading) {
            drawScreen(frontScreen, front, frontAdapter);
            drawScreen(backScreen, back, backAdapter);
            drawFade(g);
        } else {
            front.draw(g);
        }
    }

    @Override
    public void onPause()
    {
        // Fader Never Pauses
    }

    @Override
    public void onResume()
    {
        // Fader Never Pauses
    }

    private void drawScreen(PGraphics screen, ArtObject obj, PGraphicAdapter adapter)
    {
        screen.beginDraw();
        obj.draw(adapter);
        screen.endDraw();
    }

    private void drawFade(DrawingObject g)
    {
        float fadeAmount = PApplet.lerp(0, 255, (parent.millis() - fadeStart) / FADE_DURATION);
        g.image(frontScreen, 255-fadeAmount);
        g.image(backScreen, fadeAmount);

        if (fadeAmount >= 255) {
           front.onPause();
           front = back;
           fading = false;
        }
    }
}
