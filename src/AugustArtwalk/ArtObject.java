
/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */

package AugustArtwalk;

public interface ArtObject {
    void update(DrawingObject g);
    void draw(DrawingObject g);
    void onPause();
    void onResume();
}
