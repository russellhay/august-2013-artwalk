/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */

package AugustArtwalk;

import processing.core.PVector;

public interface RandomHelper
{
    float random(float max);
    int randomColor();
    PVector randomUnitVector();
    PVector randomVector();
}
