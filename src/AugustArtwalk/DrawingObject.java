/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */
package AugustArtwalk;

import processing.core.PGraphics;
import processing.core.PVector;

public interface DrawingObject {
    void background(float r, float g, float b);
    void background(float gray);
    void image(PGraphics screen, float opacity);
    void stroke(int color);
    void circle(float x, float y, int radius);
    void fill(int color);
    float width();
    float height();
    void pushMatrix();
    float halfWidth();
    float halfHeight();
    void translate(float v, float v1);
    void popMatrix();
    void noFill();

    PGraphics asPGraphic();
    void noStroke();

    void strokeWeight(int i);
}
