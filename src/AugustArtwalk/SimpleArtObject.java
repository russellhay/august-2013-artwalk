/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */

package AugustArtwalk;

public abstract class SimpleArtObject implements ArtObject
{
    @Override
    public void onPause()
    {
       // Do Nothing
    }

    @Override
    public void onResume()
    {
        // Do Nothing
    }
}
