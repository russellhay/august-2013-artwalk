/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */

package AugustArtwalk;

import processing.core.PApplet;

public class Main {

    public static void main(String args[]) {
        PApplet.main(new String[]{"--present", "AugustArtwalk.AugustArtwalk"});
    }
}
