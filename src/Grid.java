/*
 * Copyright (c) 2013 Russell Hay
 * All Rights Reserved
 */

import AugustArtwalk.ArtObject;
import AugustArtwalk.DrawingObject;
import processing.core.PApplet;

public class Grid implements ArtObject
{
    private float r,g,b;
    public Grid(float r, float g, float b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }
    @Override
    public void update(DrawingObject g) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void draw(DrawingObject g) {
        g.background(r,this.g,b);
    }

    @Override
    public void onPause()
    {
        PApplet.println("Pausing " + r + " " + g);
    }

    @Override
    public void onResume()
    {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
